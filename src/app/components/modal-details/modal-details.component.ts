import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'modal-details',
  templateUrl: './modal-details.component.html',
  styleUrls: ['./modal-details.component.scss']
})
export class ModalDetailsComponent {
  private modalRef: BsModalRef;
  @ViewChild('itemModal') _templateModal: ElementRef;
  @Input() animated: boolean;

  @Input()
  set modalState(_modalState: any) {
    if (_modalState) {
      this.openModal();
    } else { this.closeModal(); }
  }
  constructor(private modalService: BsModalService) {}

  openModal() {
    const view = this;
    // wrap the modal open in asyc function to avoid error
    setTimeout(function() {
      view.modalRef = view.modalService.show(view._templateModal, {
        backdrop: 'static',
        animated: this.animated,
        keyboard: false
      });
    }, 0);
  }

  clicked() {}

  closeModal() {
    if (this.modalRef) { this.modalRef.hide(); }
  }
}
