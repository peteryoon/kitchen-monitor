import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef
} from '@angular/core';
import { ActivatedRoute, Params, Router, Data } from '@angular/router';
import { DataStorageService } from '../../shared/data-storage.service';
import { Observable } from 'rxjs/Observable';
import { NgxSpinnerService } from 'ngx-spinner';
import { select } from '@angular-redux/store';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  @select(['staticData', 'store'])
  store$: Observable<any>;
  @select(['orders', 'new', 'count'])
  countNew$: Observable<number>;
  @select(['orders', 'ready', 'count'])
  countReady$: Observable<number>;
  @select(['orders', 'completed', 'count'])
  countDone$: Observable<number>;
  @select('loading') loading$: Observable<boolean>;
  @ViewChild('countEl') countEl: ElementRef;
  private prevCount = 0;
  private subscription: any;

  constructor(
    private dataStorageService: DataStorageService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.spinner.show();
    const view = this;
    this.subscription = this.countNew$.subscribe({
      next: e => {
        // Animate the counter badge only when it goes up (new item is there)
        if (e > view.prevCount && view.countEl) {
          view.countEl.nativeElement.className = 'count new';
          setTimeout(function() {
            view.countEl.nativeElement.className =
              'count new bounceInDown animated';
          }, 100);
        }
        view.prevCount = e;
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
