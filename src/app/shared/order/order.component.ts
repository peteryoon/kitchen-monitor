import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewEncapsulation
} from '@angular/core';
import { MainActions } from '../../store/actions';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class OrderComponent implements OnInit {
  @Input() order: any;
  @Input() status: string;
  @Output() clickChangeStatus = new EventEmitter<number>();

  constructor(public actions: MainActions) {}

  ngOnInit() {}

  changeStatus(id) {
    if (this.status != null) { this.actions.changeOrderStatus(id, this.status); }
  }

  showOrderDetails(order) {
    this.actions.selectOrder(order);
  }

  print() {
    console.log('PRINING!');
  }
}
