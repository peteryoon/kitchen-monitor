import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'options-text',
  templateUrl: './options-text.component.html',
  styleUrls: ['./options-text.component.scss']
})
export class OptionsTextComponent implements OnInit {
  @Input() options: any[];
  @Input() showOptionset: boolean;

  constructor() {}

  ngOnInit() {}
}
