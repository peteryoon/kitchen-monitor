import { Component, OnInit, Input } from '@angular/core';
import { MainActions } from '../../store/actions';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {
  @Input() order: any;
  @Input() status: string;
  constructor(public actions: MainActions) {}

  ngOnInit() {
    console.log('status', this.status);
  }

  onCloseModal() {
    this.actions.closeOrder();
  }

  onChangeStatus() {
    this.actions.changeOrderStatus(this.order.id, this.status);
  }
}
