import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { DataStorageService } from './data-storage.service';
import { MainActions } from '../store/actions';

@Injectable()
export class HomeResolver implements Resolve<any> {
  constructor(
    private dataStorageService: DataStorageService,
    private actions: MainActions
  ) {}

  // Get info from singleton. If not fetched yet then fetch only the first time loading
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    const data = this.dataStorageService.getInit();
    if (data) {
      return false;
    } else {
      console.log('FETCHING INITIAL!!!');
      return new Promise((resolve, reject) => {
        this.dataStorageService.fetchInit().subscribe(response => {
          this.actions.storeInit(response);
          resolve(response);
        });
      });
    }
  }
}
