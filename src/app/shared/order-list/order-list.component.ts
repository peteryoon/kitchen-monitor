import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  ViewChild,
  Output,
  EventEmitter
} from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';
import { MainActions } from '../../store/actions';
import { Observable } from 'rxjs/Observable';
import * as $ from 'jQuery';
import { config } from '../../../config';

@Component({
  selector: 'order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
  @select('selectedOrder') selectedOrder$: Observable<any>;
  @select('showModal') showModal$: Observable<any>;
  @ViewChild('scrollEl') scrollEl: any;
  @Input() orders: any;
  @Input() category: string;
  public page = 0;
  public nextCategory: string;

  constructor(public actions: MainActions) {}

  ngOnInit() {
    switch (this.category) {
      case 'new':
        this.nextCategory = 'ready';
        break;
      case 'ready':
        this.nextCategory = 'completed';
        break;
      case 'completed':
        this.nextCategory = 'pending';
        break;
      default:
        break;
    }
  }

  get totalPage(): number {
    return Math.floor((this.orders.count - 1) / config.pageSize);
  }

  changeStatus(id) {
    if (this.nextCategory != null) {
      this.actions.changeOrderStatus(id, this.nextCategory);
    }
  }

  scroll(id) {
    if (id > 0) { this.page++; } else { this.page--; }
    const currentPage = this.page;
    $('order-list .scroll-container').animate(
      {
        scrollTop: currentPage * 460
      },
      300
    );
  }
}
