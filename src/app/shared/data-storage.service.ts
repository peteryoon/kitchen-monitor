import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Restaurant } from './restaurant.model';
import { environment } from '../../environments/environment';

@Injectable()
export class DataStorageService {
  constructor(private httpClient: HttpClient) {}

  private main: any;
  private baseUrl = environment.apiBase;

  fetchInit() {
    if (this.main) { return this.main; }
    return this.httpClient
      .get<any>(this.baseUrl + 'init.php', {
        observe: 'body',
        responseType: 'json'
      })
      .map(data => {
        this.storeMain(data);
        return data;
      });
  }

  fetchOrders(type) {
    return this.httpClient
      .get<any>(this.baseUrl + 'getOrders.php?type=' + type, {
        observe: 'body',
        responseType: 'json'
      })
      .map(data => {
        return data;
      });
  }

  changeOrderStatus(param) {
    console.log('param', param);
    return this.httpClient
      .post<any>(this.baseUrl + 'changeStatus.php', param, {
        observe: 'body'
      })
      .map(data => {
        return data;
      });
  }

  getInit() {
    return this.main;
  }

  storeMain(res) {
    this.main = res;
  }
}
