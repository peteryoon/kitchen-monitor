import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';
import { AppState } from '../reducers';
import { DataStorageService } from '../../shared/data-storage.service';

@Injectable()
export class MainActions {
  static FETCH_INIT = 'FETCH_INIT';
  static FETCH_NEW_ORDERS = 'FETCH_NEW_ORDERS';
  static FETCH_ALL_ORDERS = 'FETCH_ALL_ORDERS';
  static LOADING = 'LOADING';
  static SELECT_ORDER = 'SELECT_ORDER';
  static CLOSE_ORDER = 'CLOSE_ORDER';
  static CHANGE_ORDER_STATUS = 'CHANGE_ORDER_STATUS';

  constructor(
    private ngRedux: NgRedux<AppState>,
    private dataService: DataStorageService
  ) {}

  storeInit(data) {
    console.log('ACTION: store initial');
    this.ngRedux.dispatch({ type: MainActions.FETCH_INIT, payload: data });
  }

  loading() {
    this.ngRedux.dispatch({ type: MainActions.LOADING });
  }

  selectOrder(item) {
    this.ngRedux.dispatch({ type: MainActions.SELECT_ORDER, payload: item });
  }

  closeOrder() {
    console.log('ACTION: close menu');
    this.ngRedux.dispatch({ type: MainActions.CLOSE_ORDER });
  }

  fetchAllOrders() {
    console.log('ACTION: fetch all orders');
    this.loading();
    this.dataService.fetchOrders('all').subscribe(response => {
      // let view = this;
      // setTimeout(function() {
      this.ngRedux.dispatch({
        type: MainActions.FETCH_ALL_ORDERS,
        payload: response
      });
      // },500);
    });
  }

  fetchNewOrders() {
    console.log('ACTION: fetch new orders');
    this.dataService.fetchOrders('pending').subscribe(response => {
      this.ngRedux.dispatch({
        type: MainActions.FETCH_NEW_ORDERS,
        payload: response
      });
    });
  }

  changeOrderStatus(orderId, status) {
    const data = {
      orderId: orderId,
      status: status
    };
    console.log('ACTION: change status of ' + orderId + ': ' + status);
    this.dataService.changeOrderStatus(data).subscribe(response => {
      this.fetchAllOrders();
    });
  }
}
