import { Action } from 'redux';
import { MainActions } from '../actions';
import * as _ from 'lodash';
import { composeReducers, defaultFormReducer } from '@angular-redux/form';
export interface AppState {
  staticData: any;
  orders: any;
  loading: boolean;
  showModal: boolean;
  selectedOrder: any;
}

export const INITIAL_STATE = {
  staticData: {},
  orders: null,
  loading: true,
  showModal: false,
  selectedOrder: null
};

function mainReducer(state = INITIAL_STATE, action): AppState {
  let item;
  switch (action.type) {
    case '@@INIT':
      item = localStorage.getItem('appState');
      // if(item) return JSON.parse(item);
      // else return INITIAL_STATE;
      break;
    // LOADING
    case MainActions.LOADING:
      return returnState({ ...state, loading: true });
    // SELECT ORDER
    case MainActions.SELECT_ORDER:
      item = _.cloneDeep(action.payload);
      return returnState({ ...state, selectedOrder: item, showModal: true });
    case MainActions.CLOSE_ORDER:
      return returnState({ ...state, showModal: false });
    // FETCH INITIAL DATA
    case MainActions.FETCH_INIT:
      return returnState({
        ...state,
        staticData: action.payload,
        loading: false
      });
    // FETCH ALL ORDERS
    case MainActions.FETCH_ALL_ORDERS:
      return returnState({
        ...state,
        orders: {
          new: action.payload.orders.newOrders,
          ready: action.payload.orders.readyOrders,
          completed: action.payload.orders.doneOrders
        },
        loading: false,
        showModal: false
      });
    // FETCH NEW ORDERS
    case MainActions.FETCH_NEW_ORDERS:
      return returnState({
        ...state,
        orders: { ...state.orders, new: action.payload.orders },
        loading: false
      });
    // CHANGE STATUS
    case MainActions.CHANGE_ORDER_STATUS:
      return state;
    /*
      return returnState({ ...state, orders: {
          new: action.payload.newOrders,
          ready: action.payload.readyOrders,
          completed: action.payload.doneOrders
        }
      });
      */

    default:
      return state;
  }
}

function returnState(state) {
  // localStorage.setItem('appState', JSON.stringify(state));
  return state;
}

export const rootReducer = composeReducers(defaultFormReducer(), mainReducer);
