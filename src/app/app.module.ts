import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {
  NgRedux,
  NgReduxModule,
  DevToolsExtension
} from '@angular-redux/store';
import { NgReduxFormModule } from '@angular-redux/form';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AppRoutingModule } from './app-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';

import { AppState, rootReducer, INITIAL_STATE } from './store/reducers';
import { MainActions } from './store/actions';
import { DataStorageService } from './shared/data-storage.service';
import { HomeResolver } from './shared/home-resolver.service';

import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { InfoComponent } from './pages/info/info.component';
import { OrderComponent } from './shared/order/order.component';
import { OrderListComponent } from './shared/order-list/order-list.component';
import { HomeComponent } from './pages/home/home.component';
import { ReadyComponent } from './pages/ready/ready.component';
import { CompletedComponent } from './pages/completed/completed.component';
import { OptionsTextComponent } from './shared/options-text/options-text.component';
import { ModalDetailsComponent } from './components/modal-details/modal-details.component';
import { OrderDetailsComponent } from './shared/order-details/order-details.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    InfoComponent,
    OrderComponent,
    OrderListComponent,
    ReadyComponent,
    CompletedComponent,
    OptionsTextComponent,
    ModalDetailsComponent,
    OrderDetailsComponent,
    ConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ModalModule.forRoot(),
    NgReduxModule,
    NgxSpinnerModule,
    NgReduxFormModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [DataStorageService, HomeResolver, MainActions],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(ngRedux: NgRedux<AppState>, private devTools: DevToolsExtension) {
    let enhancers = [];
    // ... add whatever other enhancers you want.

    // You probably only want to expose this tool in devMode.
    // if (__DEVMODE__ && devTools.isEnabled())
    {
      enhancers = [...enhancers, devTools.enhancer()];
    }
    ngRedux.configureStore(rootReducer, INITIAL_STATE, [], enhancers);
  }
}
