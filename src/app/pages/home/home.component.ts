import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';
import { ActivatedRoute, Params, Router, Data } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { routerTransition } from '../../shared/routerTransition';
import { MainActions } from '../../store/actions';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [routerTransition()],
  host: { '[@routerTransition]': '' }
})
export class HomeComponent implements OnInit, OnDestroy {
  @select(['orders', 'new'])
  newOrders$: Observable<any>;
  private timer: any;

  constructor(private route: ActivatedRoute, public actions: MainActions) {}

  ngOnInit() {
    const view = this;
    view.actions.fetchAllOrders();

    this.timer = setInterval(function() {
      view.actions.fetchNewOrders();
    }, environment.fetchTimeout);
  }

  ngOnDestroy() {
    clearInterval(this.timer);
  }
}
