import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';
import { ActivatedRoute, Params, Router, Data } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { routerTransition } from '../../shared/routerTransition';
import { MainActions } from '../../store/actions';

@Component({
  selector: 'app-completed',
  templateUrl: './completed.component.html',
  styleUrls: ['./completed.component.scss'],
  animations: [routerTransition()],
  host: { '[@routerTransition]': '' }
})
export class CompletedComponent implements OnInit, OnDestroy {
  @select(['orders', 'completed'])
  orders$: Observable<any>;
  private timer: any;

  constructor(private route: ActivatedRoute, public actions: MainActions) {}

  ngOnInit() {
    const view = this;
    view.actions.fetchAllOrders();
  }

  ngOnDestroy() {
    clearInterval(this.timer);
  }
}
