import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../shared/routerTransition';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
  animations: [routerTransition()],
  host: { '[@routerTransition]': '' }
})
export class InfoComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
